// swift-tools-version:5.5

import PackageDescription

let package = Package(
    name: "PushLite",
    products: [
        .library(
            name: "PushLite",
            targets: ["PushLite"]),
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(
            name: "PushLite",
            url: "https://artifactory.i-dgtl.ru/push-libs-ios/push-lite/3.0.6/push-lite-3.0.6.zip",
            checksum: "6d8bffcbd5d7773ea9468de8ea93a36e8777c7714364f05ac36667f7fcf78b31"
        ),
    ]
)
